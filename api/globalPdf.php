<?php 
require '../../vendor/autoload.php';
use Spipu\Html2Pdf\Html2Pdf;
class GeneratePdf{
    public static function generate($html, $path, $fileName){
        $html2pdf = new Html2Pdf('P', 'A4', 'fr');
        // Render the HTML as PDF
        $html2pdf->writeHTML($html);
        // Output the generated PDF to Browser
        $html2pdf->output(__DIR__ . '/uploads/' . $fileName . '.pdf', 'F');
        echo $path.$fileName.".pdf"; 
    }
}