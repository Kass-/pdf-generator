<?php
@include 'globalPdf.php';
 // Allow from any origin
if (isset($_SERVER["HTTP_ORIGIN"])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    } else {
    header("Access-Control-Allow-Origin: *");
    }

    header("Access-Control-Allow-Credentials: true");
    header("Access-Control-Max-Age: 600");

    if ($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
    if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))
    header("Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT");

    if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))
    header("Access-Control-Allow-Headers:
    {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

// Get Form Datas
if(isset($_POST['formData']) && $_POST['formData'] != null){
  $form = json_decode($_POST['formData']);
  $nameObj = htmlspecialchars($form->nameObj, ENT_QUOTES);
  $author = htmlspecialchars($form->author, ENT_QUOTES);
  $dimensions = htmlspecialchars($form->dimensions, ENT_QUOTES);
  $colorChoice = htmlspecialchars($form->colorChoice, ENT_QUOTES);
  $color = htmlspecialchars($form->color, ENT_QUOTES);
  $textChoice = htmlspecialchars($form->textChoice, ENT_QUOTES);
  $txtOnObj = htmlspecialchars($form->txtOnObj, ENT_QUOTES);
  $animation = htmlspecialchars($form->animation, ENT_QUOTES);
  $description = htmlspecialchars($form->description, ENT_QUOTES);
  $utility = htmlspecialchars($form->utility, ENT_QUOTES);
  $externalized = htmlspecialchars($form->externalized, ENT_QUOTES);
}

// require '../../vendor/autoload.php';
// use Spipu\Html2Pdf\Html2Pdf;

if(isset($_POST['formData'])){
    try {
        // $html2pdf = new Html2Pdf('P', 'A4', 'fr');
        $html = '<page backtop="15mm" backbottom="20mm" backleft="10mm" backright="10mm" backcolor="#FEFEFE" footer="date;time;page" style="font-size: 12pt"> 
                    <page_footer>
                    Simango - 8 Quai Robinot de Saint-Cyr, 35000 Rennes
                    </page_footer>';
        if (!empty($nameObj)) {
            $html .= '<h1>Fiche Objet : ' . $nameObj . ' </h1><br/>';
        }
        if (!empty($author)) {
            $html .= '<h3>Auteur/Autrice : ' . $author . ' </h3><br/>';
        }
        if (!empty($dimensions)) {
            $html .= '<p><b>Dimensions</b> : ' . $dimensions . ' </p><br/>';
        }
        if (!empty($colorChoice)) {
            $html .= '<p><b> Couleur :</b> ' . $colorChoice . '</p>';
        }
        if (!empty($color)) {
            $html .= '<p>Détails : ' . $color . '</p><br/>';
        }
        if (!empty($textChoice)) {
            $html .= '<p><b>Texte sur l\'objet :</b> ' . $textChoice . '</p>';
        }
        if (!empty($txtOnObj)) {
            $html .= '<p>Détails : ' . $txtOnObj . ' </p><br/>'; 
        }
        if (!empty($animation)) {
            $html .= '<p><b>Animation :</b> ' . $animation . ' </p>';
        }
        if (!empty($description)) {
            $html .= '<p><b>Description :</b> ' . $description . ' </p><br/>';
        }
        if (!empty($utility)) {
            $html .= '<p><b>Utilité de l\'objet dans le contexte demandé :</b> ' . $utility . ' </p><br/>';
        }
        if (!empty($externalized)) {
            $html .= '<p><b>Rendus attendus :</b> ' . $externalized . ' </p><br/>';
        }
        $html .= '<p><b>Références visuelles :</b> <br/> <br/>';
        // About the images
        $index = 0;
        $found = true;

        do {
            $fileNewName = checkImageAndReturnName($_FILES['image_' . $index]);
            $index++;
            if ($fileNewName) {
                $html .= "<img style=\"width:400px\" src=\"https://api-auteur.simango.fr/tests/uploads/" . $fileNewName . "\" alt=\"image\"/>";
                // $fileToDelete = __DIR__ . '/uploads/' .$fileNewName;
                $files = glob(__DIR__ . '/uploads/*');
            } else {
                $found = false;
            }
        } while ($found);
        $html .= '<br/>';
        $html .= '</p>
        </page>';
        // $html2pdf->writeHTML($html);
        // $html2pdf->output(__DIR__ . '/uploads/' . $nameObj . '.pdf', 'F');
        // echo "https://api-auteur.simango.fr/tests/uploads/$nameObj.pdf";
        GeneratePdf::generate($html,"https://api-auteur.simango.fr/tests/uploads/", $nameObj);
        foreach($files as $file){ // iterate files
            if(is_file($file)) {
              unlink($file); // delete file
            }
        }
        return;
    } catch (Exception $e) {
        echo $e;
        return;
    }
}

function checkImageAndReturnName($file) {
    $fileNewName = null;
    if(isset($file) && $file != null){
        $fileName = basename($file['name']);
        $fileTmpName = $file['tmp_name'];
        $fileSize = $file['size'];
        $fileError = $file['error'];
        $fileType = $file['type'];
        $fileExtension = explode('.', $fileName);
        $fileActualExt = strtolower(end($fileExtension));
        $allowed = array('jpg', 'png', 'jpeg', 'pdf');
        if(in_array($fileActualExt, $allowed)){
            if($fileError === 0){
                if($fileSize < 100000000){
                    $fileNewName = uniqid('', false).'.'.$fileActualExt;
                    $fileDestination = __DIR__ . '/uploads/'.$fileNewName;
                    move_uploaded_file($fileTmpName, $fileDestination);
                }else{
                    echo "Votre fichier est trop volumineux";
                    return null;
                }
            } else {
                echo "Il y a eu une erreur lors du téléchargement";
                return null;
            }
        }else{
            echo "Vous ne pouvez pas télécharger ce type de fichiers";
            return null;
        }
    }
    return $fileNewName;
}



